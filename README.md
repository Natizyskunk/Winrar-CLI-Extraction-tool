# Winrar-CLI-Extraction-tool.
Winrar (for Windows) extraction batch script by @Natizyskunk.

## How-To.
1) Placer le script "extract-XXX.bat" (remplacer 'XXX' par l'extension désirée à l'extraction) ansi que votre fichier ".XXX" (remplacer 'XXX' par l'extension désirée à l'extraction) dans le même dossier.
2) Double cliquer sur le fichier "extract-XXX.bat" pour lancer l'extraction.
3) Vous trouverez le contenu extrait dans un nouveau dossier nommé "extracted" se trouvant dans le même dossier.

## Releases-Notes.
- v1.0 => 08 mars 2018
